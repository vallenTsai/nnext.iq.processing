sap.ui.define([], function() {
	"use strict";

	return {
		processStep: function(fValue) {
			var matches = fValue.match(/[^\]@\[]+/g);
			return matches[1];
		}
	};
});