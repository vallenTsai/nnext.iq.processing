sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Folder", {
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf SplitApp.view.Folder
		 */
		onInit: function() {
			var ctrl = this;
			
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			
			$.ajax("/Flow7Api/api/diagram")
				.done(function(data){
					var oData = new sap.ui.model.json.JSONModel(data);
					ctrl.getView().setModel(oData, "diagram");
				});
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf SplitApp.view.Folder
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf SplitApp.view.Folder
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf SplitApp.view.Folder
		 */
		//	onExit: function() {
		//
		//	}
		goToList1: function() {
			this._oRouter.navTo("list1");
		},
		goToList2: function() {
			this._oRouter.navTo("list2");
		},
		
		onPress: function(oEvent){
			//console.log(oEvent);
			var oObject = oEvent.getSource().getBindingContext("diagram");
			//console.log(oObject);
			var oItem = oObject.getModel().getProperty(oObject.getPath());

			this._oRouter.navTo("tablelist", {
				folderguid: oItem.FolderGuid,
				query: {
					test: oItem.FolderGuid
				}
			});
		}
	});

});