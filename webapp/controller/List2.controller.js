sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"nnext/iq/Processing/model/Formatter"
], function(Controller, History, Filter, FilterOperator,Formatter) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.List2", {
		oFormatter: Formatter,
		onInit: function() {
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.getOwnerComponent().getRouter().getRoute("list2").attachPatternMatched(this._onRouteMatched, this);
			
		},
		_onRouteMatched: function(oEvent) {
			var sIdentify = oEvent.getParameter("arguments").identify;
			var aFilter = [];
			var ctrl = this;
			$.ajax("/Flow7Api/api/dashboard/processing")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					ctrl.getView().setModel(oData, "requisitions");
					
					aFilter.push(new Filter("Identify", FilterOperator.Contains, sIdentify));
					var oList = ctrl.getView().byId("tableProcessing");

					var oBinding = oList.getBinding("items");
					oBinding.filter(aFilter);
				});
		},
		onPress: function(oEvent) {
			var oObject = oEvent.getSource().getBindingContext("requisitions");
			var oItem = oObject.getModel().getProperty(oObject.getPath());
			this._oRouter.navTo("bucket1detail", {
				requisitionid: oItem.RequisitionId
			});
		},
		onPressList2Back: function(oEvent) {
			this._oRouter.navTo("initialized");
		}
	});
});